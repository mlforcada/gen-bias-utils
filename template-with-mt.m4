#
# This template needs to be processed into a shell by replacing:
# ENGSG with a regular expression for a singular English noun, such as [Nn]urse
# ENGPL with a regular expression for a plural English noun, such as [Nn]urses
# SPAMSG with a regular expression for a masculine singular Spanish noun such as [Ee]nfermero
# SPAMPL with a regular expression for a masculine plural Spanish noun such as [Ee]nfermeros
# SPAFSG with a regular expression for a feminine singular Spanish noun such as [Ee]nfermera
# SPAFPL with a regular expression for a feminine plural Spanish noun such as [Ee]nfermeras
# NUMBER with the base number of examples
# (this can easily be done with m4:
# define(ENGSG,[Nn]urse)dnl
# define(ENGPL,[Nn]urses)dnl
# define(SPAMSG,[Ee]nfermero)dnl
# define(SPAMPL,[Ee]nfermeros)dnl
# define(SPAFSG,[Ee]nfermera)dnl
# define(SPAFPL,[Ee]nfermeras)dnl
# define(NUMBER,1000)dnl
#
# and it will indeed be done by calling m4 via another shell.
#
# it reads as standard input a parallel corpus (Moses style), with English and Spanish
# using TAB as separator.
# © Mikel L Forcada 2020 under the GPL v3 license
# Some factoring / preprocessing of regular expressions could occur here
# or in the m4 processor to make this clearerT
# 
# )
locale=C # This is because we use bc for calculations

# Name temporary files
basename=$(mktemp) # temporary base name
total=$basename".all" # all files
source=$basename".src" # just the source
mto=$basename".mto" # machine-translated output
total_mt=$basename".allmt" # source + machine-translated output

#corpus 
masc=$basename".masc"
fem=$basename".fem"
mascfem=$basename".mascfem"
femmasc=$basename".femmasc"
# MT output
masc_mt=$basename".mascmt"
fem_mt=$basename".femmt"
mascfem_mt=$basename".mascfemmt"
femmasc_mt=$basename".femmascmt"


batchsize=1350 # empirically found to work well



# select
# - translation units containing at least one appearance of the English word on the English side
# - but no more than one
# - containing at least one appearance of the Spanish word on the Spanish side
# - and no more than NUMBER lines in translation units
paste $1 $2 | grep "\(^\|[^[:alpha:]]\)\(ENGSG\|ENGPL\)[^[:alpha:]].*[\t]" | grep -v "\(^\|[^[:alpha:]]\)\(ENGSG\|ENGPL\)[^[:alpha:]].*[^[:alpha:]]\(ENGSG\|ENGPL\)[^[:alpha:]].*[\t]" | grep "[\t]\(.*[^[:alpha:]]\)\?\(SPAMSG\|SPAFSG\|SPAMPL\|SPAFPL\)[^[:alpha:]]" | head -NUMBER  >$total

# Extract the source side, sort it by size and store it
# Sorting does not affect statistics
cut -f1 $total | awk '{ print length, $0 }' | sort -n -s | cut -d" " -f2- >$source
# Translate it using the python shell
# The 'transformers' conda environment is assumed to be activated
python translate.py $source $mto $batchsize 
# join
paste $source $mto >$total_mt



# count how many such lines we got (should be NUMBER or less)
nTot=$(cat $total | wc -l)


# Corpus statistics
# Count how many times this corresponds to a single masculine word in Spanish 

cat $total | grep "[\t].*[^[:alpha:]]\(SPAMSG\|SPAMPL\)[^[:alpha:]]" | grep -v "[^[:alpha:]]\(SPAMSG\|SPAFSG\|SPAMPL\|SPAFPL\)[^[:alpha:]].*[^[:alpha:]]\(SPAMSG\|SPAFSG\|SPAMPL\|SPAFPL\)[^[:alpha:]]" >$masc
nMasc=$(cat $masc | wc -l)

# Count how many times this corresponds to a single feminine word in Spanish
cat $total | grep "[\t].*[^[:alpha:]]\(SPAFSG\|SPAFPL\)[^[:alpha:]]" | grep -v "[^[:alpha:]]\(SPAMSG\|SPAFSG\|SPAMPL\|SPAFPL\)[^[:alpha:]].*[^[:alpha:]]\(SPAMSG\|SPAFSG\|SPAMPL\|SPAFPL\)[^[:alpha:]]" >$fem
nFem=$(cat $fem | wc -l)

# Number of times appearing as a doublet masc and/or fem (no triplet)
cat $total | grep "[\t].*[^[:alpha:]]\(SPAMSG\|SPAMPL\)\([^[:alpha:]]\+[eyou][^[:alpha:]]\+\|,[^[:alpha:]]\+\)\(SPAFSG\|SPAFPL\)[^[:alpha:]]" | grep -v "[^[:alpha:]]\(SPAMSG\|SPAFSG\|SPAMPL\|SPAFPL\)[^[:alpha:]].*[^[:alpha:]]\(SPAMSG\|SPAFSG\|SPAMPL\|SPAFPL\)[^[:alpha:]].*[^[:alpha:]]\(SPAMSG\|SPAFSG\|SPAMPL\|SPAFPL\)[^[:alpha:]]" >$mascfem
nMascFem=$(cat $mascfem | wc -l)

# Number of times appearing as a doublet fem and/or masc (no triplet)
cat $total | grep "[\t].*[^[:alpha:]]\(SPAFSG\|SPAFPL\)\([^[:alpha:]]\+[eyou][^[:alpha:]]\+\|,[^[:alpha:]]\+\)\(SPAMSG\|SPAMPL\)[^[:alpha:]]" | grep -v "[^[:alpha:]]\(SPAMSG\|SPAFSG\|SPAMPL\|SPAFPL\)[^[:alpha:]].*[^[:alpha:]]\(SPAMSG\|SPAFSG\|SPAMPL\|SPAFPL\)[^[:alpha:]].*[^[:alpha:]]\(SPAMSG\|SPAFSG\|SPAMPL\|SPAFPL\)[^[:alpha:]]" >$femmasc
nFemMasc=$(cat $femmasc | wc -l)

# MT statistics
# Count how many times this corresponds to a single masculine word in Spanish 

cat $total_mt | grep "[\t].*[^[:alpha:]]\(SPAMSG\|SPAMPL\)[^[:alpha:]]" | grep -v "[^[:alpha:]]\(SPAMSG\|SPAFSG\|SPAMPL\|SPAFPL\)[^[:alpha:]].*[^[:alpha:]]\(SPAMSG\|SPAFSG\|SPAMPL\|SPAFPL\)[^[:alpha:]]" >$masc_mt
nMascMT=$(cat $masc_mt | wc -l)

# Count how many times this corresponds to a single feminine word in Spanish
cat $total_mt | grep "[\t].*[^[:alpha:]]\(SPAFSG\|SPAFPL\)[^[:alpha:]]" | grep -v "[^[:alpha:]]\(SPAMSG\|SPAFSG\|SPAMPL\|SPAFPL\)[^[:alpha:]].*[^[:alpha:]]\(SPAMSG\|SPAFSG\|SPAMPL\|SPAFPL\)[^[:alpha:]]" >$fem_mt
nFemMT=$(cat $fem_mt | wc -l)

# Number of times appearing as a doublet masc and/or fem (no triplet)
cat $total_mt | grep "[\t].*[^[:alpha:]]\(SPAMSG\|SPAMPL\)\([^[:alpha:]]\+[eyou][^[:alpha:]]\+\|,[^[:alpha:]]\+\)\(SPAFSG\|SPAFPL\)[^[:alpha:]]" | grep -v "[^[:alpha:]]\(SPAMSG\|SPAFSG\|SPAMPL\|SPAFPL\)[^[:alpha:]].*[^[:alpha:]]\(SPAMSG\|SPAFSG\|SPAMPL\|SPAFPL\)[^[:alpha:]].*[^[:alpha:]]\(SPAMSG\|SPAFSG\|SPAMPL\|SPAFPL\)[^[:alpha:]]" >$mascfem_mt
nMascFemMT=$(cat $mascfem_mt | wc -l)

# Number of times appearing as a doublet fem and/or masc (no triplet)
cat $total_mt | grep "[\t].*[^[:alpha:]]\(SPAFSG\|SPAFPL\)\([^[:alpha:]]\+[eyou][^[:alpha:]]\+\|,[^[:alpha:]]\+\)\(SPAMSG\|SPAMPL\)[^[:alpha:]]" | grep -v "[^[:alpha:]]\(SPAMSG\|SPAFSG\|SPAMPL\|SPAFPL\)[^[:alpha:]].*[^[:alpha:]]\(SPAMSG\|SPAFSG\|SPAMPL\|SPAFPL\)[^[:alpha:]].*[^[:alpha:]]\(SPAMSG\|SPAFSG\|SPAMPL\|SPAFPL\)[^[:alpha:]]" >$femmasc_mt
nFemMascMT=$(cat $femmasc_mt | wc -l)

printf "\"%15s\", \"%15s\", \"%15s\", \"%15s\", \"%15s\", \"%15s\", %6d, %6d, %6d, %6d, %6d, %6d, %6d, %6d, %6d, %6d\n" "ENGSG" "ENGPL" "SPAFSG" "SPAFPL" "SPAMSG" "SPAMPL" $nFem $nMasc $nFemMasc $nMascFem $[nTot-nMasc-nFem-nMascFem-nFemMasc] $nFemMT $nMascMT $nFemMascMT $nMascFemMT $[nTot-nMascMT-nFemMT-nMascFemMT-nFemMascMT]



if [ "$3" == "delete" ]
then
rm $total
rm $total_mt
rm $source
rm $mto
rm $masc
rm $fem
rm $femmasc
rm $mascfem
rm $masc_mt
rm $fem_mt
rm $mascfem_mt
rm $femmasc_mt
fi
