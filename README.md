# gen-bias-utils

Shells and tools to research gender bias in English-Spanish training corpora.

Currently, a shell ``study.sh`` creates a temporary shell from template ``template.m4`` to study the statistics of translations in a bilingual corpus. It takes a line with six regular expressions: English singular, English plural, Spanish feminine singular, Spanish feminine plural, Spanish masculine singular, Spanish masculine plural. 

Another version of the shell, ``study-with-mt.sh`` creates a similar temporary shell from template ``template-with-mt.m4`` to add machine translation statistics to the bilingual corpus. Machine translation is performed using Tensorflow starting from HuggingFace models.


