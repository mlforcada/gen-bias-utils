import argparse
from transformers import AutoTokenizer, AutoModelForSeq2SeqLM


parser=argparse.ArgumentParser(description="Read input and output file")
parser.add_argument("source",help="source file")
parser.add_argument("target",help="target file")
parser.add_argument("size",help="batch size in 'words'", type=int)
args=parser.parse_args()

# Load translation model
tokenizer = AutoTokenizer.from_pretrained("Helsinki-NLP/opus-mt-en-es")
model = AutoModelForSeq2SeqLM.from_pretrained("Helsinki-NLP/opus-mt-en-es")
# Prepare a set of sentences (generates tensors with integers representing word pieces, I guess)
source=open(args.source,"r")
target=open(args.target,"w")
large_enough=args.size
lines=source.readlines()
batch=[] 
size=0
nlines=len(lines)
for pos, line in enumerate(lines) :
   # approximate size calculation
   size=size+len(line.split())
   batch=batch+[line]
   if size>large_enough or pos==nlines-1: 
      batch=tokenizer.prepare_seq2seq_batch(src_texts=batch)
      # run it through the generator (this generates another matrix with integers, etc)
      gen=model.generate(**batch)
      # generate list of translations
      result=tokenizer.batch_decode(gen,skip_special_tokens=True)
      # and print
      for outline in result :
         target.write("%s\n" % outline)
      batch=[]
      size=0
source.close()
target.close()
exit
